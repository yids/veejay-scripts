#!/usr/bin/env bash

set -eux

# this starts a fake X server with mesa opengl, normally you would use a real one with a gpu, but this server does not have one
# it will be used by virtualgl to do its rendering on

#Xorg -noreset +extension SECURITY +extension GLX +extension RANDR +extension RENDER -logfile fakexlogs/0.log -config ./fake-xorg.conf -logverbose 4 :1
Xorg -noreset +extension GLX +extension RANDR +extension RENDER -logfile fakexlogs/0.log -config ./forg.conf -logverbose 4 :23
DISPLAY=:1 xhost +

