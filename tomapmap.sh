#!/bin/bash


v4l_output_width=2048
v4l_output_height=1536
v4lcaps="video/x-raw,format=YUY2,width=$v4l_output_width,height=$v4l_output_height"


#sudo rmmod v4l2aloopback
#sudo modprobe  v4l2loopback video_nr=2,3,4,5

/home/y/src/streaming/v4l2loopback/utils/./v4l2loopback-ctl set-caps $v4lcaps

for (( c=2; c<6; c++ ))
do
  gst-launch-1.0 filesrc location=/tmp/pipe$c ! decodebin  ! videoconvert  ! v4l2sink device=/dev/video$c &
  sleep 3
done


#gst-launch-1.0 filesrc location=/tmp/pipe1 ! decodebin  ! videoconvert  ! v4l2sink device=/dev/video2 &
#gst-launch-1.0 filesrc location=/tmp/pipe2 ! decodebin  ! videoconvert  ! v4l2sink device=/dev/video3 &
#gst-launch-1.0 filesrc location=/tmp/pipe2 ! decodebin  ! videoconvert  ! v4l2sink device=/dev/video4 &
#gst-launch-1.0 filesrc location=/tmp/pipe2 ! decodebin  ! videoconvert  ! v4l2sink device=/dev/video5 &



#gst-launch-1.0 -vv filesrc location=/tmp/pipe1 \
#        ! decodebin \
#        ! videoconvert \
#        ! v4l2sink device=/dev/video2


#gst-launch-1.0 -vv \
#         videomixer name=mix sink_0::xpos=0 sink_0::ypos=0 sink_1::xpos=1024 sink_1::ypos=0 sink_2::xpos=0 sink_2::ypos=768 sink_3::xpos=1024 sink_3::ypos=768 \
#        ! "video/x-raw, format=(string)I420, width=(int)2048, height=(int)1536" \
#        ! xvimagesink \
#         videotestsrc \
#        ! video/x-raw,width=1024,height=768  \
#        ! mix.sink_0 \
#         videotestsrc pattern="ball" \
#        ! video/x-raw,width=1024,height=768 \
#        ! mix.sink_1
#         videotestsrc pattern="snow" \
#        ! video/x-raw,width=1024,height=768 \
#        ! mix.sink_2
#         videotestsrc pattern="red" \
#        ! video/x-raw,width=1024,height=768 \
#        ! mix.sink_3
#
#gst-launch-1.0 -vvv \
#         videomixer name=mix sink_0::xpos=0 sink_0::ypos=0 sink_1::xpos=1024 sink_1::ypos=0 sink_2::xpos=0 sink_2::ypos=768 sink_3::xpos=1024 sink_3::ypos=768 \
#        ! "video/x-raw, format=(string)I420, width=(int)2048, height=(int)1536" \
#        ! videoconvert \
#        ! v4l2sink max-lateness=-1 device=/dev/video2 \
#         filesrc location=/tmp/pipe1 \
#        ! queue \
#        ! decodebin \
#        ! videoconvert \
#        ! mix.sink_0 \
#         filesrc location=/tmp/pipe2 \
#        ! queue \
#        ! decodebin \
#        ! videoconvert \
#        ! mix.sink_1 \
#         filesrc location=/tmp/pipe3 \
#        ! queue \
#        ! decodebin \
#        ! videoconvert \
#        ! mix.sink_2 \
#         filesrc location=/tmp/pipe4 \
#        ! queue \
#        ! decodebin \
#        ! videoconvert \
#        ! mix.sink_3 \



