#!/bin/bash

pipe=/tmp/pipe
dev=/dev/video2
working_dir=/home/y/vj/veejay
numscreens=1
let loop_offset=numscreens+2
width=1024
height=576
#width=1280
#height=720
v4lcaps="video/x-raw,format=YUY2,width=$width,height=$height,framerate=25/1"

function info()
{
  echo "veejay setup with settings:"
  echo "num displays: $numscreens"
  echo "caps: $v4lcaps"
  echo "veejay line: veejay -w $width -h $height -a0 --output 4 --output-file /tmp/pipe$c"
  echo "gstreamer pipeline: gst-launch-1.0 filesrc location=/tmp/pipe$c ! queue  ! decodebin  ! queue  ! videoconvert ! queue  ! $v4lcaps  ! v4l2sink device=/dev/video$c " 
 
  
  read
}
#rm $pipe
#mkfifo $pipe
#
#yuv4mpeg_to_v4l2 $dev < $pipe &
#
#veejay -a 0 --output 4 --output-file $pipe
#
#killall veejay

function setup()
{
#  sudo rmmod v4l2loopback --force
#  sleep 1
#  sudo modprobe  v4l2loopback video_nr=2,3,4,5
#  sleep 1
#  for (( c=2; c<$loop_offset; c++ ))
#  do
#    /home/y/src/streaming/v4l2loopback/utils/./v4l2loopback-ctl set-caps $v4lcaps /dev/video$c
#  done
  if [[ -n $(lsmod | grep v4l2loopback) ]]; 
  then 
          echo "loaded"; 
  else 
          sudo modprobe v4l2loopback video_nr=4,5,6,7 card_label="v4l2loopback1","v4l2loopback2","v4l2loopback3","v4l2loopback4" && echo "v4l2loopback module not loaded, loading...";  
  fi

  for pid in $(ps aux | grep veejay | grep output | awk '{print $2}'); do kill -9 $pid; done
  rm ~/.veejay/*.shm_id
}

# this function is called when Ctrl-C is sent
function trap_ctrlc ()
{
    # perform cleanup here
  echo "Ctrl-C caught...performing clean up"

  echo "Doing cleanup"

  for (( c=2; c<$loop_offset; c++ ))
  do  
    rm /tmp/pipe$c  
  done
  # exit shell script with error code 2
  # if omitted, shell script will continue execution

  for pid in $(ps aux | grep veejay | grep output | awk '{print $2}'); do kill -9 $pid; done
  rm ~/.veejay/*.shm_id

  exit 2
}

# initialise trap to call trap_ctrlc function
# when signal 2 (SIGINT) is received
trap "trap_ctrlc" 2
function start_veejays()
{
  #while [[ true ]]; do
  for (( c=2; c<$loop_offset; c++ ))
  do
    mkfifo /tmp/pipe$c
    veejay -w $width -h $height -a0 --output 4 --output-file /tmp/pipe$c &
  done
  killall gst-launch-1.0
}

function start_pipelines()
{
  for (( c=2; c<$loop_offset; c++ ))                                                                                                                                                                                                     
  do
    gst-launch-1.0 filesrc location=/tmp/pipe$c ! queue  ! decodebin  ! queue  ! videoconvert ! queue  ! $v4lcaps  ! v4l2sink device=/dev/video$c  &
    sleep 3
  done
}
function start_single()
{
  rm -rf /tmp/pipe
  mkfifo /tmp/pipe 
  veejay -w $width -h $height -a0 --output 4 --output-file /tmp/pipe &
  sleep 5
  yuv4mpeg_to_v4l2 /dev/video5 < /tmp/pipe &
  sleep 2
}
cd $working_dir
#echo $loop_offset
info
setup
#start_veejays
#sleep 10 
#start_pipelines
#sleep 5
start_single
reloaded -S -f &

while [[ true ]]; do
   sleep 1
done

